package it.cannizzaro.itsm.url;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static it.cannizzaro.itsm.utils.ServiceNowUtils.pingServiceNowHost;

public abstract class BaseIntegrationTest {


    protected static String baseUrl;
    protected static String user;
    protected static String password;


    @BeforeAll
    static void init() {
        initPropertiesFile();

        boolean pingResult = pingServiceNowHost(baseUrl, user, password);
        Assumptions.assumeTrue(pingResult);
    }

    private static void initPropertiesFile() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/integration/resources/integration_tests.properties"));
        } catch (IOException e) {
            Assertions.fail(e);
        }

        baseUrl = properties.getProperty("service.now.base.url");
        user = properties.getProperty("service.now.user");
        password = properties.getProperty("service.now.password");

    }


}


