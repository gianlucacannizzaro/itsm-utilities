package it.cannizzaro.itsm.url;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.query.expression.Expression;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static it.cannizzaro.itsm.utils.ServiceNowFields.System.CREATION_DATE;
import static it.cannizzaro.itsm.utils.ServiceNowFields.System.ID;
import static it.cannizzaro.itsm.enumeration.SystemTable.GROUP;
import static it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper.*;
import static it.cannizzaro.itsm.url.IntegrationTestUtils.getInactiveGroup;
import static it.cannizzaro.itsm.url.IntegrationTestUtils.getIntegrationTestGroup;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromListResponse;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromSingletonResponse;

public class SearchTest extends BaseIntegrationTest {

    private static String integrationGroupId;
    private static String inactiveGroupId;


    @BeforeAll
    static void setUp() throws JsonProcessingException, ItsmException {
        String insertResult = new InsertURL(baseUrl,
                GROUP)
                .basicAuth(user, password)
                .body(getIntegrationTestGroup())
                .send();

        integrationGroupId = getFieldFromSingletonResponse(insertResult, ID);

        insertResult = new InsertURL(baseUrl,
                GROUP)
                .basicAuth(user, password)
                .body(getInactiveGroup())
                .send();


        inactiveGroupId = getFieldFromSingletonResponse(insertResult, ID);
    }

    @AfterAll
    static void cleanUp() throws ItsmException {
        DeleteByIdURL url = new DeleteByIdURL(
                baseUrl,
                GROUP,
                integrationGroupId)
                .basicAuth(user, password);

        url.send();

        url = new DeleteByIdURL(
                baseUrl,
                GROUP,
                inactiveGroupId)
                .basicAuth(user, password);

        url.send();

    }

    @Test
    public void searchesGroupsWithQueryFilterCorrectly() throws ItsmException, JsonProcessingException {

        String listResult = new ListURL(baseUrl, GROUP)
                .query(and(
                        isFalse("active"),
                        after(CREATION_DATE,
                                LocalDateTime.of(2010, Month.JANUARY, 1, 0, 0, 0))))
                .fields(ID)
                .basicAuth(user, password)
                .send();

        String foundId = getFieldFromListResponse(listResult, ID, 0);

        Assertions.assertEquals(inactiveGroupId, foundId);

    }

    @Test
    public void searchesGroupsWithPaginationCorrectly() throws ItsmException, JsonProcessingException {

        String listResult = new ListURL(baseUrl, GROUP)
                .query(getSearchExpression())
                .pageSize(1L)
                .fields(ID)
                .basicAuth(user, password)
                .send();

        assertSearch(listResult);

    }

    @Test
    public void searchesGroupsWithoutPaginationCorrectly() throws ItsmException, JsonProcessingException {

        String listResult = new ListURL(baseUrl, GROUP)
                .query(getSearchExpression())
                .fields(ID)
                .basicAuth(user, password)
                .send();

        assertSearch(listResult);

    }

    private void assertSearch(String listResult) throws JsonProcessingException {
        List<String> actual = Stream.of(
                        getFieldFromListResponse(listResult, ID, 0),
                        getFieldFromListResponse(listResult, ID, 1))
                .sorted()
                .collect(Collectors.toList());

        List<String> expected = Stream.of(integrationGroupId, inactiveGroupId)
                .sorted()
                .collect(Collectors.toList());


        Assertions.assertEquals(expected, actual);
    }

    private Expression getSearchExpression() {
        return or(is("name", "Integration test group"), is("name", "Inactive group"));
    }

}
