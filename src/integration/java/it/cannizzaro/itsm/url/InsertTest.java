package it.cannizzaro.itsm.url;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.cannizzaro.itsm.exception.ItsmException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.utils.ServiceNowFields.System.ID;
import static it.cannizzaro.itsm.enumeration.SystemTable.GROUP;
import static it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper.is;
import static it.cannizzaro.itsm.url.IntegrationTestUtils.getIntegrationTestGroup;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromListResponse;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromSingletonResponse;

public class InsertTest extends BaseIntegrationTest {


    private String insertedId;

    @Test
    public void insertsANewGroupCorrectly() throws ItsmException, JsonProcessingException {


        String insertResult = new InsertURL(baseUrl,
                GROUP)
                .basicAuth(user, password)
                .body(getIntegrationTestGroup())
                .send();


        insertedId = getFieldFromSingletonResponse(insertResult, ID);


        String findResult = new ListURL(baseUrl, GROUP)
                .query(is("name", "Integration test group"))
                .basicAuth(user, password)
                .send();

        String foundId = getFieldFromListResponse(findResult, ID, 0);

        Assertions.assertEquals(insertedId, foundId);

    }

    @AfterEach
    public void cleanUp() throws ItsmException {
        DeleteByIdURL url = new DeleteByIdURL(
                baseUrl,
                GROUP,
                insertedId)
                .basicAuth(user, password);

        url.send();


    }

}
