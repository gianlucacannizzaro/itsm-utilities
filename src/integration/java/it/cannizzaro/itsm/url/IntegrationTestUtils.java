package it.cannizzaro.itsm.url;

public class IntegrationTestUtils {

    public static String getIntegrationTestGroup() {
        return "{\n" +
                "  \"active\": \"true\",\n" +
                "  \"description\": \"Integration test group\",\n" +
                "  \"name\": \"Integration test group\",\n" +
                "  \"exclude_manager\": \"false\",\n" +
                "  \"email\": \"integration@test.com\",\n" +
                "  \"include_members\": \"false\",\n" +
                "  \"sys_created_by\": \"admin\"\n" +
                "}";
    }

    public static String getInactiveGroup() {
        return "{\n" +
                "  \"name\": \"Inactive group\",\n" +
                "  \"active\": \"false\"\n" +
                "}";
    }


}
