package it.cannizzaro.itsm.url;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.cannizzaro.itsm.exception.ItsmException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.utils.ServiceNowFields.System.ID;
import static it.cannizzaro.itsm.enumeration.SystemTable.GROUP;
import static it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper.is;
import static it.cannizzaro.itsm.url.IntegrationTestUtils.getIntegrationTestGroup;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromListResponse;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromSingletonResponse;

public class UpdateTest extends BaseIntegrationTest {


    private String idToUpdate;


    @BeforeEach
    public void setUp() throws JsonProcessingException, ItsmException {
        String insertResult = new InsertURL(baseUrl,
                GROUP)
                .basicAuth(user, password)
                .body(getIntegrationTestGroup())
                .send();


        idToUpdate = getFieldFromSingletonResponse(insertResult, ID);
    }


    @Test
    public void updatesAGroupCorrectly() throws ItsmException, JsonProcessingException {

        Assertions.assertDoesNotThrow(() -> {
            new UpdateByIdURL(baseUrl, GROUP, idToUpdate)
                    .body("{\"name\":\"Updated name\"}")
                    .basicAuth(user, password)
                    .send();
        });

        String listResult = new ListURL(baseUrl, GROUP)
                .query(is("name", "Updated name"))
                .basicAuth(user, password)
                .send();

        String idFound = getFieldFromListResponse(listResult, ID, 0);


        Assertions.assertEquals(idToUpdate, idFound);

    }

    @AfterEach
    public void cleanUp() throws ItsmException {
        DeleteByIdURL url = new DeleteByIdURL(
                baseUrl,
                GROUP,
                idToUpdate)
                .basicAuth(user, password);

        url.send();


    }

}
