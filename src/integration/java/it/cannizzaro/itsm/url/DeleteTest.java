package it.cannizzaro.itsm.url;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.cannizzaro.itsm.exception.ItsmException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.utils.ServiceNowFields.System.ID;
import static it.cannizzaro.itsm.enumeration.SystemTable.GROUP;
import static it.cannizzaro.itsm.url.IntegrationTestUtils.getIntegrationTestGroup;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getFieldFromSingletonResponse;

public class DeleteTest extends BaseIntegrationTest {


    private String idToDelete;


    @BeforeEach
    public void setUp() throws JsonProcessingException, ItsmException {
        String insertResult = new InsertURL(baseUrl,
                GROUP)
                .basicAuth(user, password)
                .body(getIntegrationTestGroup())
                .send();


        idToDelete = getFieldFromSingletonResponse(insertResult, ID);
    }


    @Test
    public void deletesAGroupCorrectly() throws ItsmException, JsonProcessingException {

        Assertions.assertDoesNotThrow(() -> {
            new DeleteByIdURL(baseUrl, GROUP, idToDelete)
                    .basicAuth(user, password)
                    .send();
        });

        ItsmException elementNotFoundException = Assertions.assertThrows(ItsmException.class, () -> {
            new FindByIdURL(baseUrl, GROUP, idToDelete)
                    .basicAuth(user, password)
                    .send();
        });

        Assertions.assertEquals(404, elementNotFoundException.getHttpErrorCode());

    }


}
