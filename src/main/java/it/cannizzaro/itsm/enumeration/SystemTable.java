package it.cannizzaro.itsm.enumeration;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum SystemTable {

    TASK("task"),
    INCIDENT("incident"),
    REQUEST("sc_req_item"),
    PROBLEM("problem"),
    WORK_NOTE("sys_journal_field"),
    METRIC("metric_instance"),
    USER("sys_user"),
    USER_ROLE("sys_user_has_role"),
    GROUP("sys_user_group"),
    DICTIONARY("sys_dictionary");


    @NonNull
    private String keyword;
}
