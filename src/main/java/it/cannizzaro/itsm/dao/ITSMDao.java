package it.cannizzaro.itsm.dao;

import it.cannizzaro.itsm.query.expression.Expression;

import java.util.List;

public interface ITSMDao<T> {

    List<T> select(Expression expression);

    T insert(T entity);

    T update(T entity);

    void delete(T entity);


}
