package it.cannizzaro.itsm.exception;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class ItsmException extends Exception {
    @NonNull
    private Integer httpErrorCode;
    @NonNull
    private String errorMessage;

    @Override
    public String getMessage() {
        return String.format("%d - %s", httpErrorCode, errorMessage);
    }
}
