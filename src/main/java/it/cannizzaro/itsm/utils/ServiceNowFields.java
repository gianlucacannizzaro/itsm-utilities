package it.cannizzaro.itsm.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class ServiceNowFields {
    @RequiredArgsConstructor
    @Getter
    public enum System {

        ID("sys_id"),
        CREATION_DATE("sys_created_on"),
        CREATED_BY("sys_created_by"),
        UPDATE_DATE("sys_updated_on"),
        UPDATED_BY("sys_updated_by");


        @NonNull
        private final String keyword;
    }

    @RequiredArgsConstructor
    @Getter
    public enum Users {

        USERNAME("user_name"),
        DEPARTMENT("department");

        @NonNull
        private final String keyword;
    }


    @RequiredArgsConstructor
    @Getter
    public enum Tasks {

        ASSIGNMENT_GROUP("assignment_group"),
        NUMBER("number");

        @NonNull
        private final String keyword;
    }

    @RequiredArgsConstructor
    @Getter
    public enum WorkNotes {

        ELEMENT_ID("element_id"),
        ELEMENT("element"),
        VALUE("value");

        @NonNull
        private final String keyword;

        @RequiredArgsConstructor
        @Getter
        public enum Elements {
            WORK_NOTES("work_notes"),
            COMMENTS("comments"),
            APPROVAL_HISTORY("approval_history");
            @NonNull
            private final String keyword;
        }
    }

    @RequiredArgsConstructor
    @Getter
    public enum Metrics {

        FIELD("field"),
        FIELD_VALUE("field_value");

        @NonNull
        private final String keyword;
    }

    @RequiredArgsConstructor
    @Getter
    public enum Groups {

        NAME("name");

        @NonNull
        private final String keyword;
    }

}
