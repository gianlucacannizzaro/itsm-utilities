package it.cannizzaro.itsm.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.url.ListURL;

import java.util.*;

import static it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper.is;

public class ServiceNowUtils {
    public static String getFieldFromSingletonResponse(String json, String field) throws JsonProcessingException {

        JsonNode result = getResult(json);

        return handleResult(result, field);
    }

    public static String getFieldFromSingletonResponse(String json, ServiceNowFields.System field) throws JsonProcessingException {
        return getFieldFromSingletonResponse(json, field.getKeyword());
    }

    public static String getFieldFromListResponse(String json, String field, int index) throws JsonProcessingException {

        JsonNode result = getResult(json, index);

        return handleResult(result, field);

    }

    public static Set<String> getFieldFromListResponse(String json, String field) throws JsonProcessingException {
        JsonNode result = getResult(json);

        Set<String> output = new HashSet<>();

        for (JsonNode arrayItem : result) {
            output.add(handleResult(arrayItem, field));
        }

        return output;
    }

    private static JsonNode getResult(String json, Integer index) throws JsonProcessingException {
        JsonNode result = new ObjectMapper()
                .readTree(json)
                .get("result");

        if (!Objects.isNull(index)) {
            result = result.get(index);
        }

        return result;
    }

    public static JsonNode getResult(String json) throws JsonProcessingException {
        return getResult(json, null);
    }

    public static ArrayNode getResult(JsonNode node) {
        return (ArrayNode) node.get("result");
    }

    private static String handleResult(JsonNode result, String field) {

        String output = null;

        if (!Objects.isNull(result)) {
            JsonNode jsonField = result.get(field);

            if (!Objects.isNull(jsonField))
                output = jsonField.asText();
        }

        return output;
    }

    public static String getFieldFromListResponse(String json, ServiceNowFields.System field, int index) throws JsonProcessingException {
        return getFieldFromListResponse(json, field.getKeyword(), index);
    }

    public static String getNoMatchesJson() {
        return "{\"result\":[]}";
    }

    public static Boolean pingServiceNowHost(String baseUrl, String username, String password) {

        String testQueryResult = "";

        try {
            testQueryResult = new ListURL(baseUrl, "incident")
                    .query(is("number", "INC999999999"))
                    .basicAuth(username, password)
                    .send();
        } catch (ItsmException e) {
            //Do nothing
        }

        return testQueryResult.equals(getNoMatchesJson());
    }
}
