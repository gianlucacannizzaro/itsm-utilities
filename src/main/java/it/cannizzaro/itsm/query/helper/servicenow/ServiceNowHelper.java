package it.cannizzaro.itsm.query.helper.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.operation.BaseFieldDelegate;
import it.cannizzaro.itsm.query.operation.DateFieldDelegate;
import it.cannizzaro.itsm.query.operation.LogicDelegate;
import it.cannizzaro.itsm.query.operation.StringFieldDelegate;
import it.cannizzaro.itsm.query.operation.servicenow.*;
import it.cannizzaro.itsm.utils.ServiceNowFields;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ServiceNowHelper {

    private static final BaseFieldDelegate baseFieldDelegate;
    private static final StringFieldDelegate stringFieldDelegate;
    private static final DateFieldDelegate dateFieldDelegate;
    private static final LogicDelegate logicDelegate;

    static {
        baseFieldDelegate = new ServiceNowBaseFieldDelegate();
        stringFieldDelegate = new ServiceNowStringDelegate();
        dateFieldDelegate = new ServiceNowDateFieldDelegate();
        logicDelegate = new ServiceNowLogicDelegate();
    }


    public static Expression is(String field, Object value) {
        return baseFieldDelegate.is(field, value);
    }

    public static Expression isTrue(String field) {
        return baseFieldDelegate.is(field, Boolean.TRUE);
    }

    public static Expression isFalse(String field) {
        return baseFieldDelegate.is(field, Boolean.FALSE);
    }


    public static Expression isNot(String field, Object value) {
        return baseFieldDelegate.isNot(field, value);
    }


    public static Expression isAnything(String fieldName) {
        return baseFieldDelegate.isAnything(fieldName);
    }


    public static Expression isSame(String fieldName, String otherFieldName) {
        return baseFieldDelegate.isSame(fieldName, otherFieldName);
    }


    public static Expression isDifferent(String fieldName, String otherFieldName) {
        return baseFieldDelegate.isDifferent(fieldName, otherFieldName);
    }


    public static Expression isEmpty(String fieldName) {
        return baseFieldDelegate.isEmpty(fieldName);
    }


    public static Expression isNotEmpty(String fieldName) {
        return baseFieldDelegate.isNotEmpty(fieldName);
    }

    public static Expression orderBy(String fieldName, ServiceNowSortOrder sortOrder) {
        return baseFieldDelegate.orderBy(fieldName, sortOrder);
    }

    public static Expression contains(String field, String value) {
        return stringFieldDelegate.contains(field, value);
    }


    public static Expression doesNotContain(String field, String value) {
        return stringFieldDelegate.doesNotContain(field, value);
    }


    public static Expression startsWith(String field, String value) {
        return stringFieldDelegate.startsWith(field, value);
    }


    public static Expression endsWith(String field, String value) {
        return stringFieldDelegate.endsWith(field, value);
    }


    public static Expression on(String dateFieldName, LocalDate date) {
        return dateFieldDelegate.on(dateFieldName, date);
    }


    public static Expression notOn(String dateFieldName, LocalDate date) {
        return dateFieldDelegate.notOn(dateFieldName, date);
    }


    public static Expression before(String dateFieldName, LocalDateTime date) {
        return dateFieldDelegate.before(dateFieldName, date);
    }


    public static Expression atOrBefore(String dateFieldName, LocalDateTime date) {
        return dateFieldDelegate.atOrBefore(dateFieldName, date);
    }


    public static Expression after(String dateFieldName, LocalDateTime date) {
        return dateFieldDelegate.after(dateFieldName, date);
    }


    public static Expression atOrAfter(String dateFieldName, LocalDateTime date) {
        return dateFieldDelegate.atOrAfter(dateFieldName, date);
    }


    public static Expression between(String dateFieldName, LocalDateTime from, LocalDateTime to) {
        return dateFieldDelegate.between(dateFieldName, from, to);
    }

    public static Expression and(Expression... args) {
        return logicDelegate.and(args);
    }

    public static Expression or(Expression... args) {
        return logicDelegate.or(args);
    }

    public static Expression topLevelOr(Expression... args) {
        return logicDelegate.topLevelOr(args);
    }

    public static Expression not(Expression arg) {
        return logicDelegate.not(arg);
    }

    public static Expression is(ServiceNowFields.System field, Object value) {
        return is(field.getKeyword(), value);
    }

    public static Expression isTrue(ServiceNowFields.System field) {
        return isTrue(field.getKeyword());
    }

    public static Expression isFalse(ServiceNowFields.System field) {
        return isFalse(field.getKeyword());
    }

    public static Expression isNot(ServiceNowFields.System field, Object value) {
        return isNot(field.getKeyword(), value);
    }

    public static Expression isAnything(ServiceNowFields.System fieldName) {
        return isAnything(fieldName.getKeyword());
    }

    public static Expression isSame(ServiceNowFields.System fieldName, ServiceNowFields.System otherFieldName) {
        return isSame(fieldName.getKeyword(), otherFieldName.getKeyword());
    }

    public static Expression isDifferent(ServiceNowFields.System fieldName, ServiceNowFields.System otherFieldName) {
        return isDifferent(fieldName.getKeyword(), otherFieldName.getKeyword());
    }

    public static Expression isEmpty(ServiceNowFields.System fieldName) {
        return isEmpty(fieldName.getKeyword());
    }

    public static Expression isNotEmpty(ServiceNowFields.System fieldName) {
        return isNotEmpty(fieldName.getKeyword());
    }

    public static Expression orderBy(ServiceNowFields.System fieldName, ServiceNowSortOrder sortOrder) {
        return orderBy(fieldName.getKeyword(), sortOrder);
    }

    public static Expression contains(ServiceNowFields.System field, String value) {
        return contains(field.getKeyword(), value);
    }

    public static Expression doesNotContain(ServiceNowFields.System field, String value) {
        return doesNotContain(field.getKeyword(), value);
    }

    public static Expression startsWith(ServiceNowFields.System field, String value) {
        return startsWith(field.getKeyword(), value);
    }

    public static Expression endsWith(ServiceNowFields.System field, String value) {
        return endsWith(field.getKeyword(), value);
    }

    public static Expression on(ServiceNowFields.System dateFieldName, LocalDate date) {
        return on(dateFieldName.getKeyword(), date);
    }

    public static Expression notOn(ServiceNowFields.System dateFieldName, LocalDate date) {
        return notOn(dateFieldName.getKeyword(), date);
    }

    public static Expression before(ServiceNowFields.System dateFieldName, LocalDateTime date) {
        return before(dateFieldName.getKeyword(), date);
    }

    public static Expression atOrBefore(ServiceNowFields.System dateFieldName, LocalDateTime date) {
        return atOrBefore(dateFieldName.getKeyword(), date);
    }

    public static Expression after(ServiceNowFields.System dateFieldName, LocalDateTime date) {
        return after(dateFieldName.getKeyword(), date);
    }

    public static Expression atOrAfter(ServiceNowFields.System dateFieldName, LocalDateTime date) {
        return atOrAfter(dateFieldName.getKeyword(), date);
    }

    public static Expression between(ServiceNowFields.System dateFieldName, LocalDateTime from, LocalDateTime to) {
        return between(dateFieldName.getKeyword(), from, to);
    }

}
