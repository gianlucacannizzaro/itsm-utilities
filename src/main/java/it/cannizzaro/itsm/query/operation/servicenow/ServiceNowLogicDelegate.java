package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.operation.LogicDelegate;

import java.util.StringJoiner;

public class ServiceNowLogicDelegate implements LogicDelegate {

    @Override
    public Expression and(Expression... args) {
        return join(ServiceNowOperator.AND, args);
    }

    @Override
    public Expression or(Expression... args) {
        return join(ServiceNowOperator.OR, args);
    }

    @Override
    public Expression topLevelOr(Expression... args) {
        return join(ServiceNowOperator.TOP_LEVEL_OR, args);
    }

    @Override
    public Expression not(Expression arg) {
        throw new UnsupportedOperationException("Service Now API does not support the 'not' (!) operator." +
                " See https://developer.servicenow.com/dev.do#!/reference/api/sandiego/rest/c_TableAPI ");
    }

    private Expression join(ServiceNowOperator operator, Expression... expressions) {

        StringJoiner sj = new StringJoiner(operator.getKeyword());

        for (Expression e : expressions) {
            sj.add(e.getExpression());
        }

        return new ServiceNowExpression(sj.toString());

    }
}
