package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.operation.BaseFieldDelegate;

public class ServiceNowBaseFieldDelegate implements BaseFieldDelegate {

    @Override
    public Expression is(String field, Object value) {
        return ServiceNowDelegateUtils.generateFieldExpression(field, ServiceNowOperator.EQUAL, value);
    }

    @Override
    public Expression isNot(String field, Object value) {
        return ServiceNowDelegateUtils.generateFieldExpression(field, ServiceNowOperator.NOT_EQUAL, value);
    }

    @Override
    public Expression isAnything(String fieldName) {
        return ServiceNowDelegateUtils.generateFieldExpression(fieldName, ServiceNowOperator.IS_ANYTHING);
    }

    @Override
    public Expression isSame(String fieldName, String otherFieldName) {
        return ServiceNowDelegateUtils.generateFieldExpression(fieldName, ServiceNowOperator.IS_SAME, otherFieldName);
    }

    @Override
    public Expression isDifferent(String fieldName, String otherFieldName) {
        return ServiceNowDelegateUtils.generateFieldExpression(fieldName, ServiceNowOperator.IS_DIFFERENT, otherFieldName);
    }

    @Override
    public Expression isEmpty(String fieldName) {
        return ServiceNowDelegateUtils.generateFieldExpression(fieldName, ServiceNowOperator.IS_EMPTY);
    }

    @Override
    public Expression isNotEmpty(String fieldName) {
        return ServiceNowDelegateUtils.generateFieldExpression(fieldName, ServiceNowOperator.IS_NOT_EMPTY);
    }

    @Override
    public Expression orderBy(String fieldName, ServiceNowSortOrder sortOrder) {

        ServiceNowOperator sortOperator;

        switch (sortOrder) {

            case DESC:
                sortOperator = ServiceNowOperator.ORDER_BY_DESC;
                break;
            case ASC:
            default:
                sortOperator = ServiceNowOperator.ORDER_BY_ASC;

        }

        return ServiceNowDelegateUtils.generateFieldExpression(sortOperator, fieldName);

    }
}
