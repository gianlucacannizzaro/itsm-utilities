package it.cannizzaro.itsm.query.operation;

import it.cannizzaro.itsm.query.expression.Expression;

/**
 * This interface specifies all the operators that must be implemented to create or combine expressions
 *
 * @author Gianluca Cannizzaro
 * @version 1.0.0
 * @since 2022-06-17
 */
public interface StringFieldDelegate {


    Expression contains(String field, String value);

    Expression doesNotContain(String field, String value);

    Expression startsWith(String field, String value);

    Expression endsWith(String field, String value);

}
