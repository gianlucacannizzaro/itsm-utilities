package it.cannizzaro.itsm.query.operation;

import it.cannizzaro.itsm.query.expression.Expression;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DateFieldDelegate {

    Expression on(String dateFieldName, LocalDate date);

    Expression notOn(String dateFieldName, LocalDate date);

    Expression before(String dateFieldName, LocalDateTime date);

    Expression atOrBefore(String dateFieldName, LocalDateTime date);

    Expression after(String dateFieldName, LocalDateTime date);

    Expression atOrAfter(String dateFieldName, LocalDateTime date);

    Expression between(String dateFieldName, LocalDateTime from, LocalDateTime to);


}
