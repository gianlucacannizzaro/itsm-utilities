package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.operation.StringFieldDelegate;

import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowDelegateUtils.generateFieldExpression;
import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowOperator.*;

/**
 * Concrete implementation of the Operation interface. Combines and creates expressions using the syntax specified by
 * the ServiceNow Table API
 *
 * @author Gianluca Cannizzaro
 * @version 1.0.0
 * @see <a href="https://developer.servicenow.com/dev.do#!/reference/api/sandiego/rest/c_TableAPI"></a>
 * @since 2022-06-17
 */
public class ServiceNowStringDelegate implements StringFieldDelegate {


    @Override
    public Expression contains(String field, String value) {
        return generateFieldExpression(field, LIKE, value);
    }

    @Override
    public Expression doesNotContain(String field, String value) {
        return generateFieldExpression(field, NOT_LIKE, value);
    }

    @Override
    public Expression startsWith(String field, String value) {
        return generateFieldExpression(field, STARTS_WITH, value);
    }

    @Override
    public Expression endsWith(String field, String value) {
        return generateFieldExpression(field, ENDSWITH, value);
    }


}
