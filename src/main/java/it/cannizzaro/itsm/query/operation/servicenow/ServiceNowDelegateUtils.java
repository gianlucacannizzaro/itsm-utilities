package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;

public class ServiceNowDelegateUtils {
    public static Expression generateFieldExpression(String field, ServiceNowOperator operator, Object value) {

        String expression = field + operator.getKeyword() + handleInput(value);
        return new ServiceNowExpression(expression);

    }

    public static Expression generateFieldExpression(String field, ServiceNowOperator operator) {

        return generateFieldExpression(field, operator, "");

    }

    public static Expression generateFieldExpression(ServiceNowOperator operator, String field) {

        return generateFieldExpression("", operator, field);

    }

    private static String handleInput(Object value) {

        String valueToPrint;

        if (value instanceof String) {
            valueToPrint = (String) value;
        } else {
            valueToPrint = value.toString();
        }

        return valueToPrint;
    }
}
