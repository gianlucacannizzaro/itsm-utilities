package it.cannizzaro.itsm.query.operation;

import it.cannizzaro.itsm.query.expression.Expression;

public interface LogicDelegate {

    Expression and(Expression... args);

    Expression or(Expression... args);

    Expression topLevelOr(Expression... args);

    Expression not(Expression arg);
}
