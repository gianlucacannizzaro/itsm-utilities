package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.operation.DateFieldDelegate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.StringJoiner;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

public class ServiceNowDateFieldDelegate implements DateFieldDelegate {


    private final DateTimeFormatter dateFormatter = ISO_LOCAL_DATE;
    private final DateTimeFormatter timeFormatter = ISO_LOCAL_TIME;

    @Override
    public Expression on(String dateFieldName, LocalDate date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.ON, fullDayInterval(date));
    }

    @Override
    public Expression notOn(String dateFieldName, LocalDate date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.NOT_ON, fullDayInterval(date));
    }

    @Override
    public Expression before(String dateFieldName, LocalDateTime date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.LESS_THAN, formatData(date));
    }

    @Override
    public Expression atOrBefore(String dateFieldName, LocalDateTime date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.LESS_THAN_OR_EQUAL, formatData(date));
    }

    @Override
    public Expression after(String dateFieldName, LocalDateTime date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.GREATER_THAN, formatData(date));
    }

    @Override
    public Expression atOrAfter(String dateFieldName, LocalDateTime date) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.GREATER_THAN_OR_EQUAL, formatData(date));
    }

    @Override
    public Expression between(String dateFieldName, LocalDateTime from, LocalDateTime to) {
        return ServiceNowDelegateUtils.generateFieldExpression(dateFieldName, ServiceNowOperator.BETWEEN, interval(from, to));
    }


    private String fullDayInterval(LocalDate date) {
        return new StringJoiner("@")
                .add(dateFormatter.format(date))
                .add(startOfDay(date))
                .add(endOfDay(date))
                .toString();
    }

    private String interval(LocalDateTime from, LocalDateTime to) {
        return new StringJoiner("@")
                .add(formatData(from))
                .add(formatData(to))
                .toString();
    }

    private String formatData(LocalDateTime localDate) {

        return formatData(dateFormatter.format(localDate), timeFormatter.format(localDate));
    }

    private String startOfDay(LocalDate localDate) {

        return formatData(dateFormatter.format(localDate), "start");
    }

    private String endOfDay(LocalDate localDate) {

        return formatData(dateFormatter.format(localDate), "end");
    }

    private String formatData(String date, String time) {

        return String.format("javascript:gs.dateGenerate('%s','%s')", date, time);
    }

}
