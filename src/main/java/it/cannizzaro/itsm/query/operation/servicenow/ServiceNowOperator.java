package it.cannizzaro.itsm.query.operation.servicenow;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ServiceNowOperator {

    AND("^"),
    OR("^OR"),
    TOP_LEVEL_OR("^NQ"),
    EQUAL("="),
    NOT_EQUAL("!="),
    LIKE("LIKE"),
    NOT_LIKE("NOT_LIKE"),
    STARTS_WITH("STARTSWITH"),
    ENDSWITH("ENDSWITH"),
    ON("ON"),
    NOT_ON("NOTON"),
    GREATER_THAN(">"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN("<"),
    LESS_THAN_OR_EQUAL("<="),
    BETWEEN("BETWEEN"),
    IS_EMPTY("ISEMPTY"),
    IS_NOT_EMPTY("ISNOTEMPTY"),
    IS_ANYTHING("ANYTHING"),
    IS_SAME("SAMEAS"),
    IS_DIFFERENT("NSAMEAS"),
    IS_EMPTY_STRING("EMPTYSTRING"),
    ORDER_BY_ASC("ORDERBY"),
    ORDER_BY_DESC("ORDERBYDESC");


    @NonNull
    private final String keyword;

}
