package it.cannizzaro.itsm.query.operation;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder;

public interface BaseFieldDelegate {

    Expression is(String field, Object value);

    Expression isNot(String field, Object value);

    Expression isAnything(String fieldName);

    Expression isSame(String fieldName, String otherFieldName);

    Expression isDifferent(String fieldName, String otherFieldName);

    Expression isEmpty(String fieldName);

    Expression isNotEmpty(String fieldName);

    Expression orderBy(String fieldName, ServiceNowSortOrder sortOrder);

}
