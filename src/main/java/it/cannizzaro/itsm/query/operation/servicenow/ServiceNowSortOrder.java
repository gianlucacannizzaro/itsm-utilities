package it.cannizzaro.itsm.query.operation.servicenow;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public enum ServiceNowSortOrder {

    ASC,
    DESC

}
