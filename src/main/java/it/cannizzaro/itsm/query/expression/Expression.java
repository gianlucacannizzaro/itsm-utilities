package it.cannizzaro.itsm.query.expression;

public interface Expression {

    String getExpression();

}
