package it.cannizzaro.itsm.query.expression.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@RequiredArgsConstructor
@EqualsAndHashCode
public class ServiceNowExpression implements Expression {

    @NonNull
    @EqualsAndHashCode.Include
    @ToString.Include
    private final String expression;

    @Override
    public String getExpression() {
        return expression;
    }
}
