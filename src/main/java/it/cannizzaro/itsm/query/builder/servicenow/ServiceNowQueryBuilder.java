package it.cannizzaro.itsm.query.builder.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.operation.BaseFieldDelegate;
import it.cannizzaro.itsm.query.operation.LogicDelegate;
import it.cannizzaro.itsm.query.operation.servicenow.ServiceNowBaseFieldDelegate;
import it.cannizzaro.itsm.query.operation.servicenow.ServiceNowLogicDelegate;
import it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder;

import java.util.ArrayList;
import java.util.List;

public class ServiceNowQueryBuilder {

    private final LogicDelegate logicDelegate;
    private final BaseFieldDelegate baseFieldDelegate;
    private Expression expression;

    public ServiceNowQueryBuilder() {
        this.expression = new ServiceNowExpression("");
        this.logicDelegate = new ServiceNowLogicDelegate();
        this.baseFieldDelegate = new ServiceNowBaseFieldDelegate();
    }

    public ChildBuilder mustBeTrue(Expression arg) {

        return mustAllBeTrue(arg);
    }

    public ChildBuilder mustAllBeTrue(Expression... args) {

        List<Expression> helper = new ArrayList<>(List.of(args));
        this.expression = logicDelegate.and(helper.toArray(args));


        return new ChildBuilder(this.expression);
    }

    public Expression build() {
        return expression;
    }

    public static class ChildBuilder {

        private final ServiceNowQueryBuilder builder = new ServiceNowQueryBuilder();

        ChildBuilder(Expression expression) {
            builder.expression = expression;
        }

        public ServiceNowQueryBuilder.ChildBuilder orMustBeTrue(Expression expression) {
            return orMustAllBeTrue(expression);
        }

        public ServiceNowQueryBuilder.ChildBuilder orMustAllBeTrue(Expression... args) {
            builder.expression = builder.logicDelegate.topLevelOr(builder.expression, builder.logicDelegate.and(args));
            return this;
        }

        public ServiceNowQueryBuilder.ChildBuilder orderBy(String fieldName, ServiceNowSortOrder sortOrder) {
            builder.expression = builder.logicDelegate.and(builder.expression, builder.baseFieldDelegate.orderBy(fieldName, sortOrder));
            return this;
        }

        public Expression build() {
            return builder.build();
        }
    }
}
