package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import lombok.NonNull;

import static it.cannizzaro.itsm.url.enumeration.RestMethod.DELETE;


public class DeleteByIdURL extends BaseURL {

    private final String id;

    public DeleteByIdURL(String baseUrl, String tableName, String id) {
        super(baseUrl, tableName);
        this.id = id;
    }

    public DeleteByIdURL(String baseUrl, SystemTable tableName, String id) {
        this(baseUrl, tableName.getKeyword(), id);
    }

    @Override
    protected void initializeParams() {
        fillParam(Param.NO_DOMAIN, Boolean.FALSE);

    }

    @Override
    protected String getPathSuffix() {
        return String.format("%s/%s", tableName, id);
    }

    @Override
    public DeleteByIdURL includeUnauthorizedDomains(@NonNull Boolean includeUnauthorizedDomains) {
        return (DeleteByIdURL) super.includeUnauthorizedDomains(includeUnauthorizedDomains);
    }

    @Override
    protected RestMethod getRestMethod() {
        return DELETE;
    }

    @Override
    public DeleteByIdURL basicAuth(@NonNull String user, @NonNull String password) {
        return (DeleteByIdURL) super.basicAuth(user, password);
    }

    @Override
    protected Boolean isResponseCodeValid(int code) {
        return code == 204;
    }
}
