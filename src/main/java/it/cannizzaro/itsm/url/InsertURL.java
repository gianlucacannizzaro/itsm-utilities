package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import lombok.NonNull;

import static it.cannizzaro.itsm.url.enumeration.RestMethod.POST;
import static it.cannizzaro.itsm.url.enumeration.UserInterfaceView.BOTH;

public class InsertURL extends BaseURL {

    public InsertURL(String baseUrl, String tableName) {
        super(baseUrl, tableName);
    }

    public InsertURL(String baseUrl, SystemTable tableName) {
        super(baseUrl, tableName.getKeyword());
    }

    @Override
    protected String getPathSuffix() {
        return tableName;
    }

    @Override
    protected void initializeParams() {
        fillParam(Param.DISPLAY_VALUES, DisplayValues.FALSE.getKeyword());
        fillParam(Param.EXCLUDE_REFERENCE_LINK, Boolean.FALSE);
        fillParam(Param.INPUT_DISPLAY_VALUES, Boolean.FALSE);
        fillParam(Param.VIEW, BOTH.getKeyword());
    }


    @Override
    public InsertURL showDisplayValues(@NonNull DisplayValues showDisplayValues) {
        return (InsertURL) super.showDisplayValues(showDisplayValues);
    }

    @Override
    public InsertURL excludeReferenceLink(@NonNull Boolean excludeReferenceLink) {
        return (InsertURL) super.excludeReferenceLink(excludeReferenceLink);
    }

    @Override
    public InsertURL view(@NonNull UserInterfaceView view) {
        return (InsertURL) super.view(view);
    }

    @Override
    public InsertURL insertDisplayValues(@NonNull Boolean insertDisplayValues) {
        return (InsertURL) super.insertDisplayValues(insertDisplayValues);
    }

    @Override
    public InsertURL fields(@NonNull String... fields) {
        return (InsertURL) super.fields(fields);
    }

    @Override
    protected RestMethod getRestMethod() {
        return POST;
    }

    @Override
    public InsertURL body(@NonNull String body) {
        return (InsertURL) super.body(body);
    }

    @Override
    public InsertURL basicAuth(@NonNull String user, @NonNull String password) {
        return (InsertURL) super.basicAuth(user, password);
    }

    @Override
    protected Boolean isResponseCodeValid(int code) {
        return code == 201;
    }
}
