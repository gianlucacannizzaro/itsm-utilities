package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import it.cannizzaro.itsm.utils.ServiceNowFields;
import lombok.NonNull;

import static it.cannizzaro.itsm.url.enumeration.RestMethod.GET;
import static it.cannizzaro.itsm.url.enumeration.UserInterfaceView.BOTH;


public class FindByIdURL extends BaseURL {

    private final String id;

    public FindByIdURL(String baseUrl, String tableName, String id) {
        super(baseUrl, tableName);
        this.id = id;
    }

    public FindByIdURL(String baseUrl, SystemTable tableName, String id) {
        this(baseUrl, tableName.getKeyword(), id);
    }


    @Override
    protected void initializeParams() {
        fillParam(Param.DISPLAY_VALUES, DisplayValues.FALSE.getKeyword());
        fillParam(Param.EXCLUDE_REFERENCE_LINK, Boolean.FALSE);
        fillParam(Param.NO_DOMAIN, Boolean.FALSE);
        fillParam(Param.VIEW, BOTH.getKeyword());
    }

    @Override
    protected String getPathSuffix() {
        return String.format("%s/%s", tableName, id);
    }

    @Override
    public FindByIdURL basicAuth(@NonNull String user, @NonNull String password) {
        return (FindByIdURL) super.basicAuth(user, password);
    }

    @Override
    public FindByIdURL showDisplayValues(@NonNull DisplayValues showDisplayValues) {
        return (FindByIdURL) super.showDisplayValues(showDisplayValues);
    }

    @Override
    public FindByIdURL includeUnauthorizedDomains(@NonNull Boolean includeUnauthorizedDomains) {
        return (FindByIdURL) super.includeUnauthorizedDomains(includeUnauthorizedDomains);
    }

    @Override
    public FindByIdURL view(@NonNull UserInterfaceView view) {
        return (FindByIdURL) super.view(view);
    }

    @Override
    public FindByIdURL fields(@NonNull String... fields) {
        return (FindByIdURL) super.fields(fields);
    }

    @Override
    public FindByIdURL fields(@NonNull ServiceNowFields.System... fields) {
        return (FindByIdURL) super.fields(fields);
    }

    @Override
    public FindByIdURL excludeReferenceLink(@NonNull Boolean excludeReferenceLink) {
        return (FindByIdURL) super.excludeReferenceLink(excludeReferenceLink);
    }

    @Override
    protected RestMethod getRestMethod() {
        return GET;
    }

}
