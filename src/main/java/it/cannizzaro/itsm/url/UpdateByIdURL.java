package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import it.cannizzaro.itsm.utils.ServiceNowFields;
import lombok.NonNull;

import static it.cannizzaro.itsm.url.enumeration.RestMethod.PATCH;
import static it.cannizzaro.itsm.url.enumeration.UserInterfaceView.BOTH;


public class UpdateByIdURL extends BaseURL {

    private final String id;

    public UpdateByIdURL(String baseUrl, String tableName, String id) {
        super(baseUrl, tableName);
        this.id = id;
    }

    public UpdateByIdURL(String baseUrl, SystemTable tableName, String id) {
        this(baseUrl, tableName.getKeyword(), id);
    }

    @Override
    protected void initializeParams() {
        fillParam(Param.DISPLAY_VALUES, DisplayValues.FALSE.getKeyword());
        fillParam(Param.INPUT_DISPLAY_VALUES, Boolean.FALSE);
        fillParam(Param.NO_DOMAIN, Boolean.FALSE);
        fillParam(Param.VIEW, BOTH.getKeyword());
    }

    @Override
    protected String getPathSuffix() {
        return String.format("%s/%s", tableName, id);
    }


    @Override
    public UpdateByIdURL showDisplayValues(@NonNull DisplayValues showDisplayValues) {
        return (UpdateByIdURL) super.showDisplayValues(showDisplayValues);
    }

    @Override
    public UpdateByIdURL includeUnauthorizedDomains(@NonNull Boolean includeUnauthorizedDomains) {
        return (UpdateByIdURL) super.includeUnauthorizedDomains(includeUnauthorizedDomains);
    }

    @Override
    public UpdateByIdURL view(@NonNull UserInterfaceView view) {
        return (UpdateByIdURL) super.view(view);
    }

    @Override
    public UpdateByIdURL insertDisplayValues(@NonNull Boolean insertDisplayValues) {
        return (UpdateByIdURL) super.insertDisplayValues(insertDisplayValues);
    }

    @Override
    public UpdateByIdURL fields(@NonNull String... fields) {
        return (UpdateByIdURL) super.fields(fields);
    }

    @Override
    public UpdateByIdURL fields(@NonNull ServiceNowFields.System... fields) {
        return (UpdateByIdURL) super.fields(fields);
    }

    @Override
    public UpdateByIdURL body(@NonNull String body) {
        return (UpdateByIdURL) super.body(body);
    }

    @Override
    public UpdateByIdURL basicAuth(@NonNull String user, @NonNull String password) {
        return (UpdateByIdURL) super.basicAuth(user, password);
    }

    @Override
    protected RestMethod getRestMethod() {
        return PATCH;
    }

}
