package it.cannizzaro.itsm.url.enumeration;

public enum RestMethod {

    DELETE,
    GET,
    HEAD,
    OPTIONS,
    PATCH,
    POST,
    PUT,
    TRACE

}
