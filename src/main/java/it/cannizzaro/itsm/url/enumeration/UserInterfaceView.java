package it.cannizzaro.itsm.url.enumeration;


import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserInterfaceView {

    DESKTOP("desktop"),
    MOBILE("mobile"),
    BOTH("both");

    @NonNull
    @Getter
    private String keyword;


}
