package it.cannizzaro.itsm.url.enumeration;


import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Param {

    DISPLAY_VALUES("sysparm_display_value"),
    INPUT_DISPLAY_VALUES("sysparm_input_display_value"),
    EXCLUDE_REFERENCE_LINK("sysparm_exclude_reference_link"),
    FIELDS("sysparm_fields"),
    LIMIT("sysparm_limit"),
    NO_COUNT("sysparm_no_count"),
    OFFSET("sysparm_offset"),
    QUERY("sysparm_query"),
    CATEGORY("sysparm_query_category"),
    NO_DOMAIN("sysparm_query_no_domain"),
    SUPPRESS_PAGINATION_HEADER("sysparm_suppress_pagination_header"),
    VIEW("sysparm_view");

    @NonNull
    @Getter
    private String keyword;

}
