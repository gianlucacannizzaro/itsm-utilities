package it.cannizzaro.itsm.url.enumeration;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DisplayValues {

    TRUE("true"),
    FALSE("false"),
    ALL("all");

    @NonNull
    @Getter
    private final String keyword;


}
