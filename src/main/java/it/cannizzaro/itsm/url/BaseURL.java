package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import it.cannizzaro.itsm.utils.ServiceNowFields;
import lombok.NonNull;
import org.apache.hc.core5.net.URIBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static it.cannizzaro.itsm.url.enumeration.Param.*;


public abstract class BaseURL {

    protected String baseUrl;
    protected String tableName;

    protected String body;

    protected String basicAuth;

    protected Long rowCount;

    protected Map<String, String> params = new TreeMap<>();

    protected BaseURL(String baseUrl, String tableName) {
        this.baseUrl = baseUrl;
        this.tableName = tableName;
        initializeParams();
    }

    protected BaseURL(String baseUrl, SystemTable tableName) {
        this(baseUrl, tableName.getKeyword());
    }

    protected BaseURL showDisplayValues(@NonNull DisplayValues showDisplayValues) {
        fillParam(DISPLAY_VALUES, showDisplayValues.getKeyword());
        return this;
    }

    protected BaseURL excludeReferenceLink(@NonNull Boolean excludeReferenceLink) {
        fillParam(EXCLUDE_REFERENCE_LINK, excludeReferenceLink);
        return this;
    }

    protected BaseURL maxResults(@NonNull Long maxResults) {
        fillParam(LIMIT, maxResults);
        return this;
    }

    protected BaseURL avoidCountingResults(@NonNull Boolean avoidCountingResults) {
        fillParam(NO_COUNT, avoidCountingResults);
        return this;
    }

    protected BaseURL offset(@NonNull Long offset) {
        fillParam(OFFSET, offset);
        return this;
    }

    protected BaseURL includeUnauthorizedDomains(@NonNull Boolean includeUnauthorizedDomains) {
        fillParam(NO_DOMAIN, includeUnauthorizedDomains);
        return this;
    }

    protected BaseURL suppressPaginationHeaders(@NonNull Boolean suppressPaginationHeaders) {
        fillParam(SUPPRESS_PAGINATION_HEADER, suppressPaginationHeaders);
        return this;
    }

    protected BaseURL view(@NonNull UserInterfaceView view) {
        fillParam(VIEW, view.getKeyword());
        return this;
    }

    protected BaseURL category(@NonNull String category) {
        fillParam(CATEGORY, category);
        return this;
    }

    protected BaseURL insertDisplayValues(@NonNull Boolean insertDisplayValues) {
        fillParam(INPUT_DISPLAY_VALUES, insertDisplayValues);
        return this;
    }

    protected BaseURL fields(@NonNull String... fields) {
        if (fields.length > 0) {
            StringJoiner helper = new StringJoiner(",");
            for (String field : fields) {
                helper.add(field);
            }
            fillParam(FIELDS, helper);
        }
        return this;
    }

    protected BaseURL fields(@NonNull ServiceNowFields.System... fields) {

        String[] arr = new String[fields.length];
        arr = Arrays.stream(fields)
                .map(ServiceNowFields.System::getKeyword)
                .collect(Collectors.toList())
                .toArray(arr);

        return fields(arr);
    }


    protected BaseURL query(@NonNull Expression query) {
        fillParam(QUERY, query.getExpression());
        return this;
    }


    protected void fillParam(Param param, Object value) {
        params.put(param.getKeyword(), value.toString());
    }

    protected abstract String getPathSuffix();

    protected abstract void initializeParams();

    protected abstract RestMethod getRestMethod();

    protected Long setRowCount(HttpResponse<String> response) {
        return Long.parseLong(response.headers().firstValue("x-total-count").orElse("1"));
    }

    protected BaseURL body(@NonNull String body) {
        this.body = body;
        return this;
    }

    protected BaseURL basicAuth(@NonNull String user, @NonNull String password) {
        byte[] basic = new StringJoiner(":")
                .add(user)
                .add(password)
                .toString()
                .getBytes(StandardCharsets.UTF_8);
        this.basicAuth = Base64.getUrlEncoder().encodeToString(basic);
        return this;
    }

    public String send() throws ItsmException {

        HttpRequest.Builder builder = HttpRequest.newBuilder();

        if (Objects.isNull(body)) {
            builder = builder.method(getRestMethod().name(), HttpRequest.BodyPublishers.noBody());
        } else {
            builder = builder
                    .method(getRestMethod().name(), HttpRequest.BodyPublishers.ofString(body))
                    .header("Content-Type", "application/json");
        }


        HttpRequest request = builder
                .uri(getURI())
                .header("Authorization", "Basic " + basicAuth)
                .build();

        HttpClient client = HttpClient
                .newBuilder()
                .build();

        try {

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            if (isResponseCodeValid(response.statusCode())) {
                this.rowCount = setRowCount(response);
                return response.body();
            } else {
                throw new ItsmException(response.statusCode(), response.body());
            }

        } catch (IOException | InterruptedException e) {
            throw new ItsmException(500, e.getMessage());
        }


    }

    URI getURI() throws ItsmException {
        try {

            URIBuilder builder = new URIBuilder(this.baseUrl)
                    .appendPath("/api/now/table")
                    .appendPath(getPathSuffix());

            for (Map.Entry<String, String> entry : params.entrySet()) {
                builder.addParameter(entry.getKey(), entry.getValue());
            }

            return builder.build();

        } catch (URISyntaxException e) {
            throw new ItsmException(500, e.getMessage());
        }
    }

    protected Boolean isResponseCodeValid(int code) {
        return code == 200;
    }

    @Override
    public String toString() {
        return "{" +
                "baseUrl='" + baseUrl + '\'' +
                ", tableName='" + tableName + '\'' +
                ", body='" + body + '\'' +
                ", basicAuth='" + basicAuth + '\'' +
                ", params=" + params +
                "}";
    }
}
