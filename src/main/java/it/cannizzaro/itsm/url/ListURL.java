package it.cannizzaro.itsm.url;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import it.cannizzaro.itsm.enumeration.SystemTable;
import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.Param;
import it.cannizzaro.itsm.url.enumeration.RestMethod;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import it.cannizzaro.itsm.utils.ServiceNowFields;
import lombok.NonNull;

import java.util.Objects;

import static it.cannizzaro.itsm.url.enumeration.Param.LIMIT;
import static it.cannizzaro.itsm.url.enumeration.RestMethod.GET;
import static it.cannizzaro.itsm.utils.ServiceNowUtils.getResult;


public class ListURL extends BaseURL {

    private Long pageSize = 50L;

    public ListURL(String baseUrl, String tableName) {
        super(baseUrl, tableName);
    }

    public ListURL(String baseUrl, SystemTable tableName) {
        super(baseUrl, tableName.getKeyword());
    }

    @Override
    protected void initializeParams() {
        fillParam(Param.DISPLAY_VALUES, DisplayValues.FALSE.getKeyword());
        fillParam(Param.EXCLUDE_REFERENCE_LINK, Boolean.FALSE);
        fillParam(Param.NO_COUNT, Boolean.FALSE);
        fillParam(Param.OFFSET, 0);
        fillParam(Param.NO_DOMAIN, Boolean.FALSE);
        fillParam(Param.SUPPRESS_PAGINATION_HEADER, Boolean.FALSE);
        fillParam(Param.VIEW, UserInterfaceView.BOTH.getKeyword());
    }


    @Override
    protected String getPathSuffix() {
        return tableName;
    }


    @Override
    public ListURL showDisplayValues(@NonNull DisplayValues showDisplayValues) {
        return (ListURL) super.showDisplayValues(showDisplayValues);
    }

    @Override
    public ListURL excludeReferenceLink(@NonNull Boolean excludeReferenceLink) {
        return (ListURL) super.excludeReferenceLink(excludeReferenceLink);
    }

    @Override
    public ListURL maxResults(@NonNull Long maxResults) {
        return (ListURL) super.maxResults(maxResults);
    }

    @Override
    public ListURL avoidCountingResults(@NonNull Boolean avoidCountingResults) {
        return (ListURL) super.avoidCountingResults(avoidCountingResults);
    }

    @Override
    public ListURL offset(@NonNull Long offset) {
        return (ListURL) super.offset(offset);
    }

    @Override
    public ListURL includeUnauthorizedDomains(@NonNull Boolean includeUnauthorizedDomains) {
        return (ListURL) super.includeUnauthorizedDomains(includeUnauthorizedDomains);
    }

    @Override
    public ListURL suppressPaginationHeaders(@NonNull Boolean suppressPaginationHeaders) {
        return (ListURL) super.suppressPaginationHeaders(suppressPaginationHeaders);
    }

    @Override
    public ListURL category(@NonNull String category) {
        return (ListURL) super.category(category);
    }

    @Override
    public ListURL fields(@NonNull String... fields) {
        return (ListURL) super.fields(fields);
    }

    @Override
    public ListURL fields(@NonNull ServiceNowFields.System... fields) {
        return (ListURL) super.fields(fields);
    }

    @Override
    public ListURL query(@NonNull Expression query) {
        return (ListURL) super.query(query);
    }

    @Override
    public ListURL view(@NonNull UserInterfaceView view) {
        return (ListURL) super.view(view);
    }

    @Override
    public ListURL basicAuth(@NonNull String user, @NonNull String password) {
        return (ListURL) super.basicAuth(user, password);
    }

    @Override
    protected RestMethod getRestMethod() {
        return GET;
    }

    @Override
    public String send() throws ItsmException {

        String output;

        if (Objects.isNull(this.params.get(LIMIT.getKeyword()))) {
            output = retrieveWithPagination();
        } else {
            output = retrieveWithoutPagination();
        }

        return output;

    }

    public ListURL pageSize(Long pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    private String retrieveWithPagination() throws ItsmException {

        Long offset = 0L;

        this.maxResults(pageSize);

        String batchResult = retrieveBatch(offset);
        offset += pageSize;


        try {

            JsonNode finalOutput = new ObjectMapper().readTree(batchResult);

            while (offset < this.rowCount) {
                batchResult = retrieveBatch(offset);
                offset += pageSize;

                ArrayNode toAdd = (ArrayNode) getResult(batchResult);
                getResult(finalOutput).addAll(toAdd);
            }

            return finalOutput.toString();

        } catch (JsonProcessingException jpe) {
            throw new ItsmException(999, jpe.getMessage());
        }


    }

    private String retrieveWithoutPagination() throws ItsmException {
        return super.send();
    }

    private String retrieveBatch(Long offset) throws ItsmException {
        this.offset(offset);
        return super.send();
    }
}
