package it.cannizzaro.itsm.query.builder.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static it.cannizzaro.itsm.utils.ServiceNowFields.System.CREATED_BY;
import static it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper.*;
import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder.ASC;
import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder.DESC;

class ServiceNowQueryBuilderTest {

    private ServiceNowQueryBuilder builder;

    @BeforeEach
    void setUp() {
        builder = new ServiceNowQueryBuilder();
    }

    @Test
    void ticketsOpenedOrClosedByAdmin() {
        Expression actual = builder
                .mustBeTrue(is(CREATED_BY, "System Administrator"))
                .orMustBeTrue(is("closed_by", "System Administrator"))
                .build();


        Assertions.assertEquals(new ServiceNowExpression(
                "sys_created_by=System Administrator" +
                        "^NQ" +
                        "closed_by=System Administrator"), actual);

    }

    @Test
    void tasksAssignedToAGroupAndClosedAfterASpecificDate() {
        Expression actual = builder
                .mustAllBeTrue(is("assignment_group", "App - AM Lutech"),
                        or(is("active", "true"), isTrue("richiesta_riapertura")),
                        or(isNot("state", "Resolved"), isTrue("richiesta_riapertura")))
                .orMustAllBeTrue(is("internal_assignment_group", "App - AM Lutech"),
                        isTrue("info_fornitore"),
                        after("closed_at", LocalDateTime.of(2022, Month.JUNE, 16, 18, 0, 0)))
                .build();

        Assertions.assertEquals(new ServiceNowExpression(
                        "assignment_group=App - AM Lutech" +
                                "^" +
                                "active=true" +
                                "^OR" +
                                "richiesta_riapertura=true" +
                                "^" +
                                "state!=Resolved" +
                                "^OR" +
                                "richiesta_riapertura=true" +
                                "^NQ" +
                                "internal_assignment_group=App - AM Lutech" +
                                "^" +
                                "info_fornitore=true" +
                                "^" +
                                "closed_at>javascript:gs.dateGenerate('2022-06-16','18:00:00')"
                ),
                actual);

    }

    @Test
    void tasksClosedInASpecificTimeInterval() {
        Expression actual = builder.mustAllBeTrue(
                        or(is("sys_class_name", "incident"), is("sys_class_name", "sc_req_item")),
                        is("assignment_group", "App - AM Lutech"),
                        isFalse("active"),
                        between("closed_at",
                                LocalDateTime.of(2022, Month.MARCH, 7, 9, 0, 0),
                                LocalDateTime.of(2022, Month.MAY, 12, 18, 18, 18)))
                .build();


        Assertions.assertEquals(new ServiceNowExpression(
                "sys_class_name=incident" +
                        "^OR" +
                        "sys_class_name=sc_req_item" +
                        "^" +
                        "assignment_group=App - AM Lutech" +
                        "^" +
                        "active=false" +
                        "^" +
                        "closed_atBETWEENjavascript:gs.dateGenerate('2022-03-07','09:00:00')" +
                        "@" +
                        "javascript:gs.dateGenerate('2022-05-12','18:18:18')"), actual);
    }

    @Test
    void ticketsOrderedByCallerAscAndOpenedDateDesc() {

        Expression actual = builder
                .mustBeTrue(is("assignment_group", "App - AM Lutech"))
                .orderBy("opened_by", ASC)
                .orderBy("opened_at", DESC)
                .build();


        Assertions.assertEquals(new ServiceNowExpression(
                "assignment_group=App - AM Lutech" +
                        "^" +
                        "ORDERBYopened_by" +
                        "^" +
                        "ORDERBYDESCopened_at"), actual);

    }


}