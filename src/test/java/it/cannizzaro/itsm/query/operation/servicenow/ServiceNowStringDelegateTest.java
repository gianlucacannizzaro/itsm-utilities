package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ServiceNowStringDelegateTest {


    @Test
    void contains() {
        Expression actual = ServiceNowHelper.contains("assignment_group", "App - AM Lutech");
        assertEquals(new ServiceNowExpression("assignment_groupLIKEApp - AM Lutech"), actual);
    }

    @Test
    void doesNotContain() {
        Expression actual = ServiceNowHelper.doesNotContain("assignment_group", "App - AM Lutech");
        assertEquals(new ServiceNowExpression("assignment_groupNOT_LIKEApp - AM Lutech"), actual);
    }

    @Test
    void startsWith() {
        Expression actual = ServiceNowHelper.startsWith("assignment_group", "App - AM Lutech");
        assertEquals(new ServiceNowExpression("assignment_groupSTARTSWITHApp - AM Lutech"), actual);
    }

    @Test
    void endsWith() {
        Expression actual = ServiceNowHelper.endsWith("assignment_group", "App - AM Lutech");
        assertEquals(new ServiceNowExpression("assignment_groupENDSWITHApp - AM Lutech"), actual);
    }
}