package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ServiceNowLogicDelegateTest {


    @Test
    void and() {
        Expression actual = ServiceNowHelper.and(ServiceNowTestUtils.getAssignmentGroupExpression(), ServiceNowTestUtils.getStatusExpression());
        assertEquals(new ServiceNowExpression("assignment_group=App - AM Lutech^state=Closed"), actual);
    }

    @Test
    void or() {
        Expression actual = ServiceNowHelper.or(ServiceNowTestUtils.getAssignmentGroupExpression(), ServiceNowTestUtils.getStatusExpression());
        assertEquals(new ServiceNowExpression("assignment_group=App - AM Lutech^ORstate=Closed"), actual);
    }

    @Test
    void not() {
        assertThrows(UnsupportedOperationException.class, () -> ServiceNowHelper.not(ServiceNowTestUtils.getAssignmentGroupExpression()));
    }

    @Test
    void topLevelOr() {
        Expression actual = ServiceNowHelper.topLevelOr(ServiceNowTestUtils.getAssignmentGroupExpression(), ServiceNowTestUtils.getStatusExpression());
        assertEquals(new ServiceNowExpression("assignment_group=App - AM Lutech^NQstate=Closed"), actual);
    }
}