package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class ServiceNowTestUtils {

    public static Expression getAssignmentGroupExpression() {
        return new ServiceNowExpression("assignment_group=App - AM Lutech");

    }

    public static Expression getStatusExpression() {
        return new ServiceNowExpression("state=Closed");
    }

    public static LocalDate getSampleStartDate() {
        return LocalDate.of(2022, Month.JUNE, 13);
    }

    public static LocalDateTime getSampleStartDateTime() {
        return LocalDateTime.of(getSampleStartDate(), LocalTime.of(11, 22, 33));
    }

    public static LocalDate getSampleEndDate() {
        return LocalDate.of(2022, Month.JUNE, 30);
    }

    public static LocalDateTime getSampleEndDateTime() {
        return LocalDateTime.of(getSampleEndDate(), LocalTime.of(12, 34, 56));
    }

    public static String getSampleId() {
        return "a83820b58f723300e7e16c7827bdeed2";
    }

    public static String getDefaultCategory() {
        return "frontEndTeam";
    }

}
