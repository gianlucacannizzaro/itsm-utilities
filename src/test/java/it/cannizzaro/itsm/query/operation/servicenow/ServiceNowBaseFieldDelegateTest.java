package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder.ASC;
import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowSortOrder.DESC;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ServiceNowBaseFieldDelegateTest {

    @Test
    void is() {
        Expression actual = ServiceNowHelper.is("assignment_group", "App - AM Lutech");
        Assertions.assertEquals(ServiceNowTestUtils.getAssignmentGroupExpression(), actual);
    }

    @Test
    void isNot() {
        Expression actual = ServiceNowHelper.isNot("assignment_group", "App - AM Lutech");
        assertEquals(new ServiceNowExpression("assignment_group!=App - AM Lutech"), actual);
    }

    @Test
    void isAnything() {
        Expression actual = ServiceNowHelper.isAnything("assignment_group");
        assertEquals(new ServiceNowExpression("assignment_groupANYTHING"), actual);
    }

    @Test
    void isSame() {
        Expression actual = ServiceNowHelper.isSame("assignment_group", "caller_group");
        assertEquals(new ServiceNowExpression("assignment_groupSAMEAScaller_group"), actual);
    }

    @Test
    void isDifferent() {
        Expression actual = ServiceNowHelper.isDifferent("assignment_group", "caller_group");
        assertEquals(new ServiceNowExpression("assignment_groupNSAMEAScaller_group"), actual);
    }

    @Test
    void isEmpty() {
        Expression actual = ServiceNowHelper.isEmpty("assignment_group");
        assertEquals(new ServiceNowExpression("assignment_groupISEMPTY"), actual);
    }

    @Test
    void isNotEmpty() {
        Expression actual = ServiceNowHelper.isNotEmpty("assignment_group");
        assertEquals(new ServiceNowExpression("assignment_groupISNOTEMPTY"), actual);
    }

    @Test
    void orderByAsc() {
        Expression actual = ServiceNowHelper.orderBy("assignment_group", ASC);
        assertEquals(new ServiceNowExpression("ORDERBYassignment_group"), actual);
    }

    @Test
    void orderByDesc() {
        Expression actual = ServiceNowHelper.orderBy("assignment_group", DESC);
        assertEquals(new ServiceNowExpression("ORDERBYDESCassignment_group"), actual);
    }

}