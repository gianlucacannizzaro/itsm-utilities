package it.cannizzaro.itsm.query.operation.servicenow;

import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.query.expression.servicenow.ServiceNowExpression;
import it.cannizzaro.itsm.query.helper.servicenow.ServiceNowHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ServiceNowDateFieldDelegateTest {


    @Test
    void on() {
        Expression actual = ServiceNowHelper.on("due_date", ServiceNowTestUtils.getSampleStartDate());
        assertEquals(new ServiceNowExpression("due_dateON2022-06-13@javascript:gs.dateGenerate('2022-06-13','start')@javascript:gs.dateGenerate('2022-06-13','end')"), actual);
    }

    @Test
    void notOn() {
        Expression actual = ServiceNowHelper.notOn("due_date", ServiceNowTestUtils.getSampleStartDate());
        assertEquals(new ServiceNowExpression("due_dateNOTON2022-06-13@javascript:gs.dateGenerate('2022-06-13','start')@javascript:gs.dateGenerate('2022-06-13','end')"), actual);
    }

    @Test
    void before() {
        Expression actual = ServiceNowHelper.before("due_date", ServiceNowTestUtils.getSampleStartDateTime());
        assertEquals(new ServiceNowExpression("due_date<javascript:gs.dateGenerate('2022-06-13','11:22:33')"), actual);
    }

    @Test
    void atOrBefore() {
        Expression actual = ServiceNowHelper.atOrBefore("due_date", ServiceNowTestUtils.getSampleStartDateTime());
        assertEquals(new ServiceNowExpression("due_date<=javascript:gs.dateGenerate('2022-06-13','11:22:33')"), actual);
    }

    @Test
    void after() {
        Expression actual = ServiceNowHelper.after("due_date", ServiceNowTestUtils.getSampleStartDateTime());
        assertEquals(new ServiceNowExpression("due_date>javascript:gs.dateGenerate('2022-06-13','11:22:33')"), actual);
    }

    @Test
    void atOrAfter() {
        Expression actual = ServiceNowHelper.atOrAfter("due_date", ServiceNowTestUtils.getSampleStartDateTime());
        assertEquals(new ServiceNowExpression("due_date>=javascript:gs.dateGenerate('2022-06-13','11:22:33')"), actual);
    }

    @Test
    void between() {
        Expression actual = ServiceNowHelper.between("due_date", ServiceNowTestUtils.getSampleStartDateTime(), ServiceNowTestUtils.getSampleEndDateTime());
        assertEquals(new ServiceNowExpression("due_dateBETWEENjavascript:gs.dateGenerate('2022-06-13','11:22:33')@javascript:gs.dateGenerate('2022-06-30','12:34:56')"), actual);
    }


}