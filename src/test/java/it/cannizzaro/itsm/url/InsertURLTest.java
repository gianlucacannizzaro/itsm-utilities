package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.getBaseUrl;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.utf8UrlDecode;
import static it.cannizzaro.itsm.url.enumeration.UserInterfaceView.DESKTOP;
import static it.cannizzaro.itsm.utils.ServiceNowFields.System.CREATION_DATE;
import static it.cannizzaro.itsm.utils.ServiceNowFields.System.UPDATE_DATE;

class InsertURLTest extends BaseURLTest<InsertURL> {


    @Override
    protected InsertURL getUri() {
        return new InsertURL(getBaseUrl(), "incident");
    }

    @Test
    @Override
    public void createsQueryWithAllParametersSet() throws ItsmException {

        String expected = "https://example.servicenow.com/api/now/table/incident?"
                + "sysparm_display_value=true"
                + "&"
                + "sysparm_exclude_reference_link=true"
                + "&"
                + "sysparm_fields=sys_created_on,sys_updated_on"
                + "&"
                + "sysparm_input_display_value=true"
                + "&"
                + "sysparm_view=desktop";


        String actual = utf8UrlDecode(uri.showDisplayValues(DisplayValues.TRUE)
                .excludeReferenceLink(true)
                .insertDisplayValues(true)
                .fields(CREATION_DATE, UPDATE_DATE)
                .view(DESKTOP)
                .getURI()
                .toString());


        Assertions.assertEquals(expected, actual);


    }

    @Test
    @Override
    public void createsQueryWithSomeParametersSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident?"
                + "sysparm_display_value=true"
                + "&"
                + "sysparm_exclude_reference_link=false"
                + "&"
                + "sysparm_fields=sys_created_on,sys_updated_on"
                + "&"
                + "sysparm_input_display_value=true"
                + "&"
                + "sysparm_view=both";


        String actual = utf8UrlDecode(uri.showDisplayValues(DisplayValues.TRUE)
                .insertDisplayValues(true)
                .fields(CREATION_DATE, UPDATE_DATE)
                .getURI()
                .toString());


        Assertions.assertEquals(expected, actual);

    }

    @Test
    @Override
    public void createsQueryWithNoParameterSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident?"
                + "sysparm_display_value=false"
                + "&"
                + "sysparm_exclude_reference_link=false"
                + "&"
                + "sysparm_fields=sys_created_on,sys_updated_on"
                + "&"
                + "sysparm_input_display_value=false"
                + "&"
                + "sysparm_view=both";


        String actual = utf8UrlDecode(uri.fields(CREATION_DATE, UPDATE_DATE).getURI().toString());


        Assertions.assertEquals(expected, actual);

    }

}