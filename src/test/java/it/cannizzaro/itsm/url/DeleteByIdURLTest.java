package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.exception.ItsmException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;

import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowTestUtils.getSampleId;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.getBaseUrl;

class DeleteByIdURLTest extends BaseURLTest<DeleteByIdURL> {


    @Test
    @Override
    protected DeleteByIdURL getUri() {
        return new DeleteByIdURL(getBaseUrl(), "incident", getSampleId());
    }

    @Test
    @Override
    public void createsQueryWithSomeParametersSet() {
        //Delete queries only have one parameter, nothing to test
    }

    @Test
    @Override
    public void createsQueryWithAllParametersSet() throws ItsmException {

        String expected = "https://example.servicenow.com/api/now/table/incident/a83820b58f723300e7e16c7827bdeed2?" +
                "sysparm_query_no_domain=true";


        String actual = uri
                .includeUnauthorizedDomains(true)
                .getURI().toString();


        Assertions.assertEquals(expected, actual);


    }

    @Test
    @Override
    public void createsQueryWithNoParameterSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident/a83820b58f723300e7e16c7827bdeed2?" +
                "sysparm_query_no_domain=false";


        String actual = uri
                .getURI().toString();


        Assertions.assertEquals(expected, actual);

    }


}