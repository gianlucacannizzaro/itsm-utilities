package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.query.builder.servicenow.ServiceNowQueryBuilder;
import it.cannizzaro.itsm.query.expression.Expression;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowTestUtils.*;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.getBaseUrl;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.utf8UrlDecode;
import static it.cannizzaro.itsm.utils.ServiceNowFields.System.*;

class ListURLTest extends BaseURLTest<ListURL> {

    private Expression query;

    @BeforeEach
    public void setUp() {
        super.setUp();
        query = new ServiceNowQueryBuilder()
                .mustAllBeTrue(getAssignmentGroupExpression(), getStatusExpression())
                .build();
    }

    @Override
    protected ListURL getUri() {
        return new ListURL(getBaseUrl(), "incident");
    }

    @Test
    @Override
    public void createsQueryWithAllParametersSet() throws ItsmException {

        String expected = "https://example.servicenow.com/api/now/table/incident?" +
                "sysparm_display_value=true" +
                "&" +
                "sysparm_exclude_reference_link=true" +
                "&" +
                "sysparm_fields=sys_id,sys_created_on,sys_updated_on" +
                "&" +
                "sysparm_limit=5678" +
                "&" +
                "sysparm_no_count=true" +
                "&" +
                "sysparm_offset=4" +
                "&" +
                "sysparm_query=assignment_group=App - AM Lutech^state=Closed" +
                "&" +
                "sysparm_query_category=frontEndTeam" +
                "&" +
                "sysparm_query_no_domain=true" +
                "&" +
                "sysparm_suppress_pagination_header=true" +
                "&" +
                "sysparm_view=mobile";


        String actual = utf8UrlDecode(uri
                .query(query)
                .showDisplayValues(DisplayValues.TRUE)
                .excludeReferenceLink(true)
                .fields(ID, CREATION_DATE, UPDATE_DATE)
                .maxResults(5678L)
                .avoidCountingResults(true)
                .offset(4L)
                .includeUnauthorizedDomains(true)
                .suppressPaginationHeaders(true)
                .view(UserInterfaceView.MOBILE)
                .category(getDefaultCategory())
                .getURI().toString());


        Assertions.assertEquals(expected, actual);


    }

    @Test
    @Override
    public void createsQueryWithSomeParametersSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident?" +
                "sysparm_display_value=true" +
                "&" +
                "sysparm_exclude_reference_link=false" +
                "&" +
                "sysparm_fields=sys_id,sys_created_on" +
                "&" +
                "sysparm_limit=1234" +
                "&" +
                "sysparm_no_count=false" +
                "&" +
                "sysparm_offset=2" +
                "&" +
                "sysparm_query=assignment_group=App - AM Lutech^state=Closed" +
                "&" +
                "sysparm_query_category=frontEndTeam" +
                "&" +
                "sysparm_query_no_domain=true" +
                "&" +
                "sysparm_suppress_pagination_header=false" +
                "&" +
                "sysparm_view=desktop";


        String actual = utf8UrlDecode(uri
                .query(query)
                .showDisplayValues(DisplayValues.TRUE)
                .fields(ID, CREATION_DATE)
                .maxResults(1234L)
                .offset(2L)
                .includeUnauthorizedDomains(true)
                .view(UserInterfaceView.DESKTOP)
                .category(getDefaultCategory())
                .getURI().toString());

        Assertions.assertEquals(expected, actual);

    }

    @Test
    @Override
    public void createsQueryWithNoParameterSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident?" +
                "sysparm_display_value=false" +
                "&" +
                "sysparm_exclude_reference_link=false" +
                "&" +
                "sysparm_no_count=false" +
                "&" +
                "sysparm_offset=0" +
                "&" +
                "sysparm_query=assignment_group=App - AM Lutech^state=Closed" +
                "&" +
                "sysparm_query_category=frontEndTeam" +
                "&" +
                "sysparm_query_no_domain=false" +
                "&" +
                "sysparm_suppress_pagination_header=false" +
                "&" +
                "sysparm_view=both";


        String actual = utf8UrlDecode(uri
                .query(query)
                .category(getDefaultCategory())
                .getURI().toString());

        Assertions.assertEquals(expected, actual);

    }

}