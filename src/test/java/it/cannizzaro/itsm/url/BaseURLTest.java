package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.exception.ItsmException;
import org.junit.jupiter.api.BeforeEach;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

public abstract class BaseURLTest<U extends BaseURL> {

    protected U uri;

    protected abstract U getUri();

    @BeforeEach
    public void setUp() {
        uri = getUri();
    }


    public abstract void createsQueryWithAllParametersSet() throws ItsmException, MalformedURLException;

    public abstract void createsQueryWithSomeParametersSet() throws ItsmException;

    public abstract void createsQueryWithNoParameterSet() throws ItsmException;

}
