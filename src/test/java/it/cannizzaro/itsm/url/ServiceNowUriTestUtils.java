package it.cannizzaro.itsm.url;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class ServiceNowUriTestUtils {


    /**
     * Return a mock ServiceNow URL for testing purposes
     *
     * @return String
     */
    public static String getBaseUrl() {
        return "https://example.servicenow.com";
    }


    public static String utf8UrlDecode(String input) {
        return URLDecoder.decode(input, StandardCharsets.UTF_8);
    }
}
