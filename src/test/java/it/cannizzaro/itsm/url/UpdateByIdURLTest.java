package it.cannizzaro.itsm.url;

import it.cannizzaro.itsm.exception.ItsmException;
import it.cannizzaro.itsm.url.enumeration.DisplayValues;
import it.cannizzaro.itsm.url.enumeration.UserInterfaceView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static it.cannizzaro.itsm.query.operation.servicenow.ServiceNowTestUtils.getSampleId;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.getBaseUrl;
import static it.cannizzaro.itsm.url.ServiceNowUriTestUtils.utf8UrlDecode;
import static it.cannizzaro.itsm.utils.ServiceNowFields.System.*;

class UpdateByIdURLTest extends BaseURLTest<UpdateByIdURL> {

    @Override
    protected UpdateByIdURL getUri() {
        return new UpdateByIdURL(getBaseUrl(), "incident", getSampleId());
    }

    @Test
    @Override
    public void createsQueryWithAllParametersSet() throws ItsmException {

        String expected = "https://example.servicenow.com/api/now/table/incident/a83820b58f723300e7e16c7827bdeed2?" +
                "sysparm_display_value=true" +
                "&" +
                "sysparm_fields=sys_id,sys_created_on,sys_updated_on" +
                "&" +
                "sysparm_input_display_value=true" +
                "&" +
                "sysparm_query_no_domain=true" +
                "&" +
                "sysparm_view=desktop";


        String actual = utf8UrlDecode(uri
                .showDisplayValues(DisplayValues.TRUE)
                .insertDisplayValues(true)
                .fields(ID, CREATION_DATE, UPDATE_DATE)
                .includeUnauthorizedDomains(true)
                .view(UserInterfaceView.DESKTOP)
                .getURI().toString());


        Assertions.assertEquals(expected, actual);


    }

    @Test
    @Override
    public void createsQueryWithSomeParametersSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident/a83820b58f723300e7e16c7827bdeed2?" +
                "sysparm_display_value=true" +
                "&" +
                "sysparm_fields=sys_id,sys_created_on,sys_updated_on" +
                "&" +
                "sysparm_input_display_value=false" +
                "&" +
                "sysparm_query_no_domain=true" +
                "&" +
                "sysparm_view=both";


        String actual = utf8UrlDecode(uri
                .showDisplayValues(DisplayValues.TRUE)
                .fields(ID, CREATION_DATE, UPDATE_DATE)
                .includeUnauthorizedDomains(true)
                .getURI().toString());

        Assertions.assertEquals(expected, actual);

    }

    @Test
    @Override
    public void createsQueryWithNoParameterSet() throws ItsmException {


        String expected = "https://example.servicenow.com/api/now/table/incident/a83820b58f723300e7e16c7827bdeed2?" +
                "sysparm_display_value=false" +
                "&" +
                "sysparm_fields=sys_id,sys_created_on,sys_updated_on" +
                "&" +
                "sysparm_input_display_value=false" +
                "&" +
                "sysparm_query_no_domain=false" +
                "&" +
                "sysparm_view=both";


        String actual = utf8UrlDecode(uri
                .fields(ID, CREATION_DATE, UPDATE_DATE)
                .getURI().toString());

        Assertions.assertEquals(expected, actual);

    }

}